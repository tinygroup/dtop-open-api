package org.tinysoft.oms.api.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.tinysoft.oms.api.dto.InOrderDetailDto;
import org.tinysoft.oms.api.dto.InOrderDto;
import org.tinysoft.oms.api.dto.OutOrderDetailDto;
import org.tinysoft.oms.api.dto.OutOrderDto;
import org.tinysoft.oms.api.dto.vo.InOrderVo;
import org.tinysoft.oms.api.dto.vo.OutOrderVo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OmsClientApplication {

    private static OAuth2RestTemplate restTemplate;
    private static String CLIENT_ID = "4094688513845713478";
    private static String CLIENT_SECRET = "1044233642893678783";
    private static String GRANT_TYPE = "client_credentials";

    private static String HOST = "http://open-api.tinysoft.cc:88";

    private static String TOKEN_URL = HOST + "/oauth/token";
    private static String IN_URL = HOST + "/order/in";
    private static String OUT_URL = HOST + "/order/out";
    private static String QUERY_URL = HOST + "/order";
    private static String CANCEL_ORDER_URL = HOST + "/order/cancel";
    private static String AUTHORIZATION = "Authorization";

    static {
        ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
        resource.setClientId(CLIENT_ID);
        resource.setClientSecret(CLIENT_SECRET);
        resource.setGrantType(GRANT_TYPE);
        resource.setAccessTokenUri(TOKEN_URL);
        restTemplate = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext());
        restTemplate.setAccessTokenProvider(new ClientCredentialsAccessTokenProvider());
        restTemplate.setErrorHandler(new MyOauthErrorHandler(resource));
    }

    private static OAuth2AccessToken getAccessToken() throws Exception {
        return restTemplate.getAccessToken();
    }


    private static void query() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        ResponseEntity entity = restTemplate.exchange(QUERY_URL + "?date={date}&queryType={type}", HttpMethod.GET, requestEntity, List.class, "2017-08-15","");
    }


    private static void insertOutOrder() throws Exception {
        OutOrderDto outOrderDto = new OutOrderDto();
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        OutOrderDetailDto outOrderDetailDto = new OutOrderDetailDto();
        List<OutOrderDetailDto> outOrderDetailDtos = new ArrayList<OutOrderDetailDto>();
        outOrderDto.setOrgId("1685554974952403023");
        outOrderDto.setBizType("0");
        outOrderDto.setExpectedTime(new Date());
        outOrderDto.setImportedFlag(false);
        outOrderDto.setCustomerId("5617743380141529099");
        outOrderDto.setTransportationCondition("1");
        outOrderDto.setTransportationMeansId("1");
        outOrderDto.setOrderNo("lichao");
        outOrderDetailDto.setAmount(20);
        outOrderDetailDto.setBatchNumberId("");
        outOrderDetailDto.setOrgGoodsNo("1231232");
        outOrderDetailDto.setUnitPrice(new Double(1000));
        outOrderDetailDtos.add(outOrderDetailDto);
        outOrderDto.setOutOrderDetailDtoList(outOrderDetailDtos);
        HttpEntity httpEntity = new HttpEntity(outOrderDto, headers);
        ResponseEntity entity = restTemplate.exchange(OUT_URL, HttpMethod.POST, httpEntity, OutOrderVo.class);
    }

    private static void insertInOrder() throws Exception {
        InOrderDto inOrderDto = new InOrderDto();
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        InOrderDetailDto InOrderDetailDto = new InOrderDetailDto();
        List<InOrderDetailDto> InOrderDetailDtos = new ArrayList<InOrderDetailDto>();
        inOrderDto.setOrgId("1");
        inOrderDto.setBizType("0");
        inOrderDto.setExpectedTime(new Date());
        inOrderDto.setImportedFlag(false);
        inOrderDto.setSupplierId("1764358130184640256");
        inOrderDto.setTransportationCondition("1");
        inOrderDto.setOrderNo("lichao");
        inOrderDto.setTransportationMeansId("1");
        InOrderDetailDto.setAmount(10);
        InOrderDetailDto.setOrgGoodsNo("1231232");
        InOrderDetailDto.setUnitPrice(new Double(250));
        InOrderDetailDtos.add(InOrderDetailDto);
        inOrderDto.setInOrderDetailDtoList(InOrderDetailDtos);
        HttpEntity httpEntity = new HttpEntity(inOrderDto, headers);
        ResponseEntity entity = restTemplate.exchange((IN_URL), HttpMethod.POST, httpEntity, InOrderVo.class);
    }

    private static void completeInOrder() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        ResponseEntity entity = restTemplate.exchange(IN_URL + "/1913563390620570938", HttpMethod.PUT, requestEntity, Void.class);
    }

    private static void completeOutOrder() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        ResponseEntity entity = restTemplate.exchange(OUT_URL + "/2522397958411948059", HttpMethod.PUT, requestEntity, Void.class);
    }

    private static void cancelOrder() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, getAccessToken().getTokenType() + " " + getAccessToken().getValue());
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        ResponseEntity entity = restTemplate.exchange(CANCEL_ORDER_URL + "/7854623225584041459", HttpMethod.PUT, requestEntity, Void.class);
    }


    public static void main(String[] args) throws Exception {
        insertInOrder();
//        insertOutOrder();
//        completeInOrder();
//        completeOutOrder();
//        cancelOrder();


    }
}
