package org.tinysoft.oms.api.client;

import org.springframework.security.oauth2.client.http.OAuth2ErrorHandler;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;

public class MyOauthErrorHandler extends OAuth2ErrorHandler{

    public MyOauthErrorHandler(OAuth2ProtectedResourceDetails resource) {
        super(new MyErrorHandler(), resource);
    }
}
