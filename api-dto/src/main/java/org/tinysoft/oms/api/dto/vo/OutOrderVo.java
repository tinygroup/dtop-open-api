package org.tinysoft.oms.api.dto.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OutOrderVo implements Serializable {
    @ApiModelProperty(value = "主键Id")
    private String id;
    @ApiModelProperty(value = "单据编号")
    private String orderNo;
    @ApiModelProperty(value = "货主Id")
    private String orgId;
    @ApiModelProperty(value = "货主名称")
    private String orgName;
    @ApiModelProperty(value = "客户Id")
    private String customerId;
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    @ApiModelProperty(value = "运输条件 0冷藏运输 1不能船运")
    private String transportationCondition;
    @ApiModelProperty(value = "运输条件具体内容")
    private String transportationConditionValue;
    @ApiModelProperty(value = "预计执行时间")
    private Date expectedTime;
    @ApiModelProperty(value = "物流运输方式key(出库运输类型: 0:配送上门,1:客户自提,2:第三方物流)")
    private String transportationMeansId;
    @ApiModelProperty(value = "物流运输方式")
    private String transportationMeans;
    @ApiModelProperty(value = "订单状态 0:待确认 6订单完成 7订单取消")
    private String state;
    @ApiModelProperty(value = "业务类型 :0:销售出库,1:采购退货，2:盘亏出库，3:调拨出库")
    private String bizType;
    @ApiModelProperty(value = "业务类型值")
    private String bizTypeValue;
    @ApiModelProperty(value = "订单总金额")
    private Double totalAmount;
    @ApiModelProperty(value = "下单时间")
    private Date createTime;
    @ApiModelProperty(value = "去向地址Id")
    private String transportationAddressId;
    @ApiModelProperty(value = "去向地址")
    private String transportationAddress;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "物流中心Id")
    private String logisticsCentreId;
    @ApiModelProperty(value = "物流中心名称", hidden = true)
    private String centreName;
    @ApiModelProperty(value = "出库单详情项")
    private List<OutOrderDetailVo> outOrderDetailVoList;

    public String getLogisticsCentreId() {
        return logisticsCentreId;
    }

    public void setLogisticsCentreId(String logisticsCentreId) {
        this.logisticsCentreId = logisticsCentreId;
    }

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public List<OutOrderDetailVo> getOutOrderDetailVoList() {
        return outOrderDetailVoList;
    }

    public void setOutOrderDetailVoList(List<OutOrderDetailVo> outOrderDetailVoList) {
        this.outOrderDetailVoList = outOrderDetailVoList;
    }

    public String getTransportationMeansId() {
        return transportationMeansId;
    }

    public void setTransportationMeansId(String transportationMeansId) {
        this.transportationMeansId = transportationMeansId;
    }

    public String getTransportationMeans() {
        return transportationMeans;
    }

    public void setTransportationMeans(String transportationMeans) {
        this.transportationMeans = transportationMeans;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTransportationCondition() {
        return transportationCondition;
    }

    public void setTransportationCondition(String transportationCondition) {
        this.transportationCondition = transportationCondition;
    }

    public String getTransportationConditionValue() {
        return transportationConditionValue;
    }

    public void setTransportationConditionValue(String transportationConditionValue) {
        this.transportationConditionValue = transportationConditionValue;
    }

    public Date getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(Date expectedTime) {
        this.expectedTime = expectedTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getBizTypeValue() {
        return bizTypeValue;
    }

    public void setBizTypeValue(String bizTypeValue) {
        this.bizTypeValue = bizTypeValue;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getTransportationAddressId() {
        return transportationAddressId;
    }

    public void setTransportationAddressId(String transportationAddressId) {
        this.transportationAddressId = transportationAddressId;
    }

    public String getTransportationAddress() {
        return transportationAddress;
    }

    public void setTransportationAddress(String transportationAddress) {
        this.transportationAddress = transportationAddress;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
