package org.tinysoft.oms.api.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class InOrderDetailDto implements Serializable {

    @ApiModelProperty(value = "货主货品编号")
    private String orgGoodsNo;
    @ApiModelProperty(value = "订单的货品数量")
    private Integer amount;
    @ApiModelProperty(value = "单价")
    private Double unitPrice;

    public String getOrgGoodsNo() {
        return orgGoodsNo;
    }

    public void setOrgGoodsNo(String orgGoodsNo) {
        this.orgGoodsNo = orgGoodsNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

}
