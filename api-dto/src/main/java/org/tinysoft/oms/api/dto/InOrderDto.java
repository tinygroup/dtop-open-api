package org.tinysoft.oms.api.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class InOrderDto implements Serializable {

    @ApiModelProperty(value = "货主Id")
    private String orgId;

    @ApiModelProperty(value = "入库业务类型key:0:采购订单,1:销售退货,2:盘盈入库,3:调拨入库")
    private String bizType;

    @ApiModelProperty(value = "入库运输类型: 1:厂商送货,2:上门提货,3:客户送货")
    private String transportationMeansId;

    @ApiModelProperty(value = "来源单位ID")
    private String supplierId;

    @ApiModelProperty(value = "单据编号")
    private String orderNo;

    @ApiModelProperty(value = "来源订单号")
    private String thirdPartyNumber;

    @ApiModelProperty(value = "运输条件：0冷藏运输 1不能船运")
    private String transportationCondition;

    @ApiModelProperty(value = "是否进口：0否，1是")
    private Boolean importedFlag;

    @ApiModelProperty(value = "订单创建时间")
    private Date createTime;
    @ApiModelProperty(value = "预计入库时间")
    private Date expectedTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "货主地址ID")
    private String transportationAddressId;

    @ApiModelProperty(value = "提货地址ID")
    private String orderPickUpAddress;

    @ApiModelProperty(value = "订单中的各个货品的信息")
    private List<InOrderDetailDto> InOrderDetailDtoList;

    @ApiModelProperty(value = "物流商Id")
    private String logisticsProviderId;

    @ApiModelProperty(value = "物流中心Id")
    private String logisticsCentreId;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrderPickUpAddress() {
        return orderPickUpAddress;
    }

    public void setOrderPickUpAddress(String orderPickUpAddress) {
        this.orderPickUpAddress = orderPickUpAddress;
    }

    public String getLogisticsCentreId() {
        return logisticsCentreId;
    }

    public void setLogisticsCentreId(String logisticsCentreId) {
        this.logisticsCentreId = logisticsCentreId;
    }

    public String getLogisticsProviderId() {
        return logisticsProviderId;
    }

    public void setLogisticsProviderId(String logisticsProviderId) {
        this.logisticsProviderId = logisticsProviderId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getTransportationMeansId() {
        return transportationMeansId;
    }

    public void setTransportationMeansId(String transportationMeansId) {
        this.transportationMeansId = transportationMeansId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getThirdPartyNumber() {
        return thirdPartyNumber;
    }

    public void setThirdPartyNumber(String thirdPartyNumber) {
        this.thirdPartyNumber = thirdPartyNumber;
    }

    public String getTransportationCondition() {
        return transportationCondition;
    }

    public void setTransportationCondition(String transportationCondition) {
        this.transportationCondition = transportationCondition;
    }

    public Boolean getImportedFlag() {
        return importedFlag;
    }

    public void setImportedFlag(Boolean importedFlag) {
        this.importedFlag = importedFlag;
    }

    public Date getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(Date expectedTime) {
        this.expectedTime = expectedTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getTransportationAddressId() {
        return transportationAddressId;
    }

    public void setTransportationAddressId(String transportationAddressId) {
        this.transportationAddressId = transportationAddressId;
    }

    public List<InOrderDetailDto> getInOrderDetailDtoList() {
        return InOrderDetailDtoList;
    }

    public void setInOrderDetailDtoList(List<InOrderDetailDto> inOrderDetailDtoList) {
        InOrderDetailDtoList = inOrderDetailDtoList;
    }
}
