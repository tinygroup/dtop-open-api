package org.tinysoft.oms.api.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OutOrderDto implements Serializable {

    @ApiModelProperty(value = "货主Id")
    private String orgId;

    @ApiModelProperty(value = "业务类型(出库业务类型:0:销售出库,1:采购退货，2:盘亏出库，3:调拨出库")
    private String bizType;

    @ApiModelProperty(value = "物流运输方式(出库运输类型: 0:配送上门,1:客户自提,2:第三方物流)")
    private String transportationMeansId;

    @ApiModelProperty(value = "去向单位ID")
    private String customerId;

    @ApiModelProperty(value = "单据编号")
    private String orderNo;

    @ApiModelProperty(value = "去向订单号")
    private String thirdPartyNumber;

    @ApiModelProperty(value = "运输条件：0冷藏运输 1不能船运")
    private String transportationCondition;

    @ApiModelProperty(value = "是否进口：0否，1是")
    private Boolean importedFlag;

    @ApiModelProperty(value = "订单创建时间")
    private Date createTime;

    @ApiModelProperty(value = "预计出库时间")
    private Date expectedTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "去向地址Id")
    private String transportationAddressId;

    @ApiModelProperty(value = "订单中的各个货品的信息")
    private List<OutOrderDetailDto> outOrderDetailDtoList;

    @ApiModelProperty(value = "实际收货人")
    private String actualConsignee;

    @ApiModelProperty(value = "是否同批次(出库)")
    private Boolean sameBatchNumber;

    @ApiModelProperty(value = "物流中心Id")
    private String logisticsCentreId;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLogisticsCentreId() {
        return logisticsCentreId;
    }

    public void setLogisticsCentreId(String logisticsCentreId) {
        this.logisticsCentreId = logisticsCentreId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getTransportationMeansId() {
        return transportationMeansId;
    }

    public void setTransportationMeansId(String transportationMeansId) {
        this.transportationMeansId = transportationMeansId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getThirdPartyNumber() {
        return thirdPartyNumber;
    }

    public void setThirdPartyNumber(String thirdPartyNumber) {
        this.thirdPartyNumber = thirdPartyNumber;
    }

    public String getTransportationCondition() {
        return transportationCondition;
    }

    public void setTransportationCondition(String transportationCondition) {
        this.transportationCondition = transportationCondition;
    }

    public Boolean getImportedFlag() {
        return importedFlag;
    }

    public void setImportedFlag(Boolean importedFlag) {
        this.importedFlag = importedFlag;
    }

    public Date getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(Date expectedTime) {
        this.expectedTime = expectedTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTransportationAddressId() {
        return transportationAddressId;
    }

    public void setTransportationAddressId(String transportationAddressId) {
        this.transportationAddressId = transportationAddressId;
    }

    public List<OutOrderDetailDto> getOutOrderDetailDtoList() {
        return outOrderDetailDtoList;
    }

    public void setOutOrderDetailDtoList(List<OutOrderDetailDto> outOrderDetailDtoList) {
        this.outOrderDetailDtoList = outOrderDetailDtoList;
    }

    public String getActualConsignee() {
        return actualConsignee;
    }

    public void setActualConsignee(String actualConsignee) {
        this.actualConsignee = actualConsignee;
    }

    public Boolean getSameBatchNumber() {
        return sameBatchNumber;
    }

    public void setSameBatchNumber(Boolean sameBatchNumber) {
        this.sameBatchNumber = sameBatchNumber;
    }

}
