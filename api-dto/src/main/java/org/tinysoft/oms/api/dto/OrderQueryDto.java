package org.tinysoft.oms.api.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class OrderQueryDto implements Serializable {
    @ApiModelProperty(value = "时间点,格式YYYY-MM-dd")
    private String date;
    @ApiModelProperty(value = "查询类型 1入库新增，2入库完成，3出库新增，4出库完成")
    private String queryType;

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
