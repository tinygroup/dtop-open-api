package org.tinysoft.oms.api.dto.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class OutOrderDetailVo implements Serializable {
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "订单Id")
    private String orderId;
    @ApiModelProperty(value = "货主货品Id")
    private String orgGoodsId;
    @ApiModelProperty(value = "货品名称")
    private String name;
    @ApiModelProperty(value = "计量单位")
    private String measurementUnit;
    @ApiModelProperty(value = "订单的货品数量")
    private Integer amount;
    @ApiModelProperty(value = "单价")
    private Double unitPrice;
    @ApiModelProperty(value = "厂家名称")
    private String factoryName;
    @ApiModelProperty(value = "批号Id")
    private String batchNumberId;
    @ApiModelProperty(value = "批号")
    private String batchNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrgGoodsId() {
        return orgGoodsId;
    }

    public void setOrgGoodsId(String orgGoodsId) {
        this.orgGoodsId = orgGoodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getBatchNumberId() {
        return batchNumberId;
    }

    public void setBatchNumberId(String batchNumberId) {
        this.batchNumberId = batchNumberId;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }
}
