package org.tinysoft.oms.api.dto;

import java.io.Serializable;

/**
 * 错误请求返回对象
 * Created by hulk on 2017/6/17.
 */
public class ErrorResponse implements Serializable {

    private int code;
    private String msg;

    public ErrorResponse() {
    }

    public ErrorResponse(String msg) {
        this.msg = msg;
    }

    public ErrorResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
